<?php

require_once('../../../../../wp-load.php');
require_once("XiaoWei.php");
require_once("config2.php");
$pay = new XiaoWei($pay_config['mch_id'], $pay_config['key'], $pay_config['url']);

if ($pay->notify()) {
    if ($_POST['status'] == 1) {
        global $wpdb, $wppay_table_name;
        $total_fee = $_POST["total_fee"] * 0.01;
        $out_trade_no = $_POST["out_trade_no"];

        if (strstr($out_trade_no, 'wppay')) {
            $order = $wpdb->get_row("select * from $wppay_table_name where order_num='" . $out_trade_no . "'");
            if ($order) {
                if (!$order->order_status) {
                    $wpdb->query("UPDATE $wppay_table_name SET order_status=1 WHERE order_num = '" . $out_trade_no . "'");
                    if ($order->user_id) {
                        $data = get_post_meta($order->post_id, 'down_url', true);
                        $ppost = get_post($order->post_id);
                        erphpAddDownloadByUid($ppost->post_title, $order->post_id, $order->user_id, $total_fee * get_option('ice_proportion_alipay'), 1, $data, $ppost->post_author);
                    }
                }
            }
        } else {
            $money_info = $wpdb->get_row("select * from " . $wpdb->icemoney . " where ice_num='" . $out_trade_no . "'");
            if ($money_info) {
                if (!$money_info->ice_success) {
                    addUserMoney($money_info->ice_user_id, $total_fee * get_option('ice_proportion_alipay'));
                    $wpdb->query("UPDATE $wpdb->icemoney SET ice_money = '" . $total_fee * get_option('ice_proportion_alipay') . "',ice_success=1, ice_success_time = '" . date("Y-m-d H:i:s") . "' WHERE ice_num = '" . $out_trade_no . "'");
                }
            }
        }

        echo '{"status":0,"message":"OK"}';
    } else {
        echo '{"status":-2,"message":"OK"}';
    }
} else {
    echo '{"status":-3,"message":"OK"}';
}
exit;
?>